// ### FIRST PROJECT###
//  ViewController.swift
//  OKO
//
//  Created by Mateusz Jur on 07.08.2018.
//  Copyright © 2018 Mateusz Jur. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var currentValue: Int = 0
    @IBOutlet weak var slider: UISlider!
    var targetValue: Int = 0
    @IBOutlet weak var targetLabel: UILabel!
    var score = 0
    @IBOutlet weak var scoreLabel: UILabel!
    var round = 0
    @IBOutlet weak var roundLabel: UILabel!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentValue = lroundf(slider.value)
        StartNewGame()
        
        // wstawianie grafiki do slidera
        
        let thumbImageNormal =  #imageLiteral(resourceName: "SliderThumb-Normal") //UIImage(named: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted") //UIImage(named: "SliderThumb-Hghlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        // tworzenie "wstawki" wyglądu slidera.
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft") //UIImage(named: "SliderTrackLeft")
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight") //UIImage(named: "SliderTrackRight")
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: .normal)

        
      
    }
    
    @IBAction func StartNewGame(){
        score = 0
        round = 0
        startNewRound()
    }
    
    func updateLabels() {
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
    }
    
    func startNewRound() {
        round += 1
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
        updateLabels()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    
    @IBAction func SliderMoved(_ slider: UISlider) {
        print("The value of slider is now: \(slider.value)")
        currentValue = lroundf(slider.value)
    }
   
    @IBAction func showAlert() {
        
        let difference: Int = abs(targetValue - currentValue)
        var points = 100 - difference
       
        var title = String()
        if difference == 0 {
            title = "Super!"
            points += 100
        } else if difference < 5 {
            title = "Byłeś Blisko"
           if difference == 1 {
            points += 50
            }
        } else if difference < 10 {
            title = "Postaraj się bardziej!"
        } else {
            title = "Możesz zrobić to lepiej!"
        }
        
        score += points

        
        let message = "Zdobyłeś \(points) punktów"
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: {
            action in
                self.startNewRound()
        })
        
        alert.addAction(action)

        present(alert, animated: true, completion: nil)
       
    }

}
